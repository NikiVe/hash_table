<?php

namespace HashTable;

class Node
{
    protected ?Node $next;
    protected ?Node $prev;
    protected $key;
    protected $value;

    public function __construct($key, $value, ?Node $next = null) {
        $this->value = $value;
        $this->key = $key;
        $this->next = $next !== null ? $next : null;
        $this->prev = null;
    }

    public function getNext() {
        return $this->next;
    }

    public function getPrev() {
        return $this->prev;
    }

    public function getKey() {
        return $this->key;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    public function setNext(Node $node) {
        $node->prev = $this;
        $this->next = $node;
        return $this;
    }

    public function unsetNext() {
        $next = $this->next;
        $this->next = null;
        return $next;
    }

    public function unsetPrev() {
        $prev = $this->prev;
        $this->prev = null;
        return $prev;
    }

    public function getLastNode() {
        if ($this->next === null) {
            return $this;
        }
        return $this->next->getLastNode();

    }
}