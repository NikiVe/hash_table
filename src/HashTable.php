<?php


namespace HashTable;


use http\Exception\RuntimeException;

class HashTable
{
    protected int $size;
    protected array $table;

    public function __construct(int $size) {
        $this->size = $size;
    }

    public function insert($key, $value) {
        $node = new Node($key, $value);
        $position = $this->getHash($key);
        if ($position > $this->size - 1) {
            throw new RuntimeException();
        }
        if (isset($this->table[$position])) {
            $last = $this->table[$position]->getLastNode();
            $last->setNext($node);
        } else {
            $this->table[$position] = $node;
        }
    }

    public function getNode($key) {
        $node = null;
        $position = $this->getHash($key);

        $f = function (Node $node, $key, $f) {
            if ($node->getKey() === $key) {
                return $node;
            }
            if ($next = $node->getNext()) {
                return $f($next, $key, $f);
            }
            return false;
        };

        return $f($this->table[$position], $key, $f);
    }

    public function get($key) {
        if ($node = $this->getNode($key)) {
            return $node->getValue();
        }
        return false;
    }

    public function unset($key) {
        if ($node = $this->getNode($key)) {
            $value = $node->getValue();

            $prev = $node->getPrev();
            $next = $node->getNext();

            $position = $this->getHash($node->getKey());

            if ($next && $prev) {
                unset($node);
                $prev->setNext($next);
                return $value;
            }

            if ($next && !$prev) {
                unset($node);
                $this->table[$position] = $next;
                $next->unsetPrev();
                return $value;
            }

            if ($prev) {
                unset($node);
            }

            unset($this->table[$position]);
        }
        return false;
    }

    public function getHash($value) {
        return hexdec(substr(md5($value), 0 , 10)) % 10;
    }
}