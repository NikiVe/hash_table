<?php
include_once __DIR__ . '/vendor/autoload.php';

$animals = [
    'собака' => 1,
    'кот' => 10,
    'тюлень' => 31,
    'слон' => 3,
    'морж' => 9,
    'заяц' => 2,
    'лиса' => 5,
    'голубь' => 0,
    'жираф' => 22,
    'антилопа' => 9
];

$table = new \HashTable\HashTable(10);
foreach ($animals as $key => $animal) {
    $table->insert($key, $animal);
}

foreach ($animals as $key => $animal) {
    if ($e = $table->get($key)) {
        if ($e !== $animal) {
            throw new \Exception();
        }
    }
}

$table->unset('собака');



